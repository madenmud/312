아래 물품들 판매합니다.

ICE125ASX2 모듈 (중국산 케이스 포함) - 15만원

RCA 입력과 볼륨조절기능이 있는 케이스 포합입니다. (인티앰프처럼 사용가능)

ICE 모듈답게 가격대비 중국 앰프들에 비해 좋은 소리를 내줍니다.

원래 전용 박스는 없습니다.

택배 거래도 가능합니다. (택배비추가)

![ice](https://bytebucket.org/madenmud/312/raw/354ffdfa79c67ad1aef82137463900d6db2d3d7f/svs/imgs/20160816_220621.jpg)


SVS 12NSD 우퍼 (12인치 밀폐형) - 58만원
박스있습니다.
밀폐형이라 12인치지만 상대적으로 작은 크기입니다.
크기때문에 직거래만 합니다.

![woofer1](https://bytebucket.org/madenmud/312/raw/354ffdfa79c67ad1aef82137463900d6db2d3d7f/svs/imgs/20170202_233452.jpg)

![woofer2](https://bytebucket.org/madenmud/312/raw/354ffdfa79c67ad1aef82137463900d6db2d3d7f/svs/imgs/20171021_175742.jpg)


CHORD RCA 1M - 5만원 
포장 상태로 판매합니다.
택배 거래도 가능합니다. (택배비추가)


![chord](https://bytebucket.org/madenmud/312/raw/354ffdfa79c67ad1aef82137463900d6db2d3d7f/svs/imgs/20180101_134503.jpg)


직거래는 서울 학동역 부근에서 가능합니다.
